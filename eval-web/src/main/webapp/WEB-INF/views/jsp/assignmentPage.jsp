<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>authentication</title>

<link type="text/css" href="resources/bootstrap.css" rel="stylesheet" />
<link type="text/css" href="resources/bootstrap-theme.css"
	rel="stylesheet" />



</head>
<body>
	<h1>Authentication page</h1>


	<div class="container">

		<div class="input-group">

			<form:form id="mainForm" method="get" action="authentication"
				modelAttribute="assignmentForm">
				<div class="form-group">
					<form:label  path="currentDPX"  for="currentDpx">Currently Connected Agent : </form:label>
					<form:input path="currentDPX" readonly="true" type="text" class="form-control"
						id="currentDpx"/>
				</div>
				<input type="submit" />
				<!-- TODO : avoid onclick, prefer event attachment -->
				<button onclick="setActionAndSubmit('mainForm', 'evaluation')">
				    evaluation
				</button>
				<button onclick="setActionAndSubmit('mainForm', 'authentication')">
				    authentication
				</button>
			</form:form>
		
		</div>
	</div>
    <script type="text/javascript">
    
       //TODO change this by clean call to the js framework
       function setAction(formId, action){
           document.getElementById(formId).action = action;
       }
       function submit(formId){
           document.getElementById(formId).submit();
       }
       function setActionAndSubmit(formId, action){
           setAction(formId,action);
           submit(formId);
       }
      
    </script>
</body>
</html>