<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>authentication</title>

<link type="text/css" href="resources/bootstrap.css" rel="stylesheet" />
<link type="text/css" href="resources/bootstrap-theme.css"
	rel="stylesheet" />



</head>
<body>
	<h1>Authentication page</h1>


	<div class="container">

		<div class="input-group">

			<form:form method="get" action="authenticate"
				modelAttribute="authForm">
				<div class="form-group">
					<form:label  path="login"  for="loginInput">Login</form:label>
					<form:input path="login" type="text" class="form-control"
						id="loginInput" placeholder="Enter Login" />
				</div>
				<div class="form-group">
					<form:label path="password" for="passwordInput">Password</form:label>
					<form:input id="passwordInput" name="password" path="password" />
				</div>
				<input type="submit" />
			</form:form>

		</div>
	</div>

</body>
</html>