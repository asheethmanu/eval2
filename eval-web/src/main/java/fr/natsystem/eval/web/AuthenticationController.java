package fr.natsystem.eval.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.natsystem.eval.web.forms.AssignmentForm;
import fr.natsystem.eval.web.forms.AuthenticationForm;

@Controller
public class AuthenticationController {

	private final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);


//	
//	@Inject
//	IAuthenticator authenticator;



	@RequestMapping(value = "/authenticate", method = RequestMethod.GET)
	public String authenticate(@ModelAttribute("authForm") AuthenticationForm form, ModelMap model) {
		//TODO call authentication service
		logger.debug("authenticate");
		logger.info(form.getLogin());
		logger.info(form.getPassword());
		logger.debug(String.valueOf(model));
		//TODO load the assigned Agents and put them in the assigmnent form
		model.put("assignmentForm", new AssignmentForm());
		
		return "assignmentPage";
	}

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		logger.debug("index");
		model.addAttribute("authForm", new AuthenticationForm());
		logger.debug(String.valueOf(model));
		return "index";
	}
}