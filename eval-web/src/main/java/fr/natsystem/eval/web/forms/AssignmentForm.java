/**
 * 
 */
package fr.natsystem.eval.web.forms;

import java.io.Serializable;

/**
 * @author tbrou
 *
 */
public class AssignmentForm implements Serializable{
	
	
	
	private String currentDPX;

	
	/**
	 * @return the currentDPX
	 */
	public String getCurrentDPX() {
		return currentDPX;
	}

	/**
	 * @param currentDPX the currentDPX to set
	 */
	public void setCurrentDPX(String currentDPX) {
		this.currentDPX = currentDPX;
	}
	
	

}
