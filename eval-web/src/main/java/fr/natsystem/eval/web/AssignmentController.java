package fr.natsystem.eval.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.natsystem.eval.web.forms.AuthenticationForm;

@Controller
public class AssignmentController {

	private final Logger logger = LoggerFactory.getLogger(AssignmentController.class);


//	
//	@Inject
//	IAuthenticator authenticator;



	@RequestMapping(value = "/evaluation", method = RequestMethod.GET)
	public String authenticate(@ModelAttribute("assignmentForm") AuthenticationForm form, ModelMap model) {
		logger.debug("authenticate");
		logger.info(form.getLogin());
		logger.info(form.getPassword());
		logger.debug(String.valueOf(model));
		return "evaluationPage";
	}

	
	@RequestMapping(value = "/authentication", method = RequestMethod.GET)
	public String index(Model model) {
		logger.debug("index");
		model.addAttribute("authForm", new AuthenticationForm());
		logger.debug(String.valueOf(model));
		return "index";
	}
}