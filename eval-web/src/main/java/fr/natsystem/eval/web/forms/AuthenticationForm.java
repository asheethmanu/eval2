/**
 * 
 */
package fr.natsystem.eval.web.forms;

import java.io.Serializable;

/**
 * @author tbrou
 *
 */
public class AuthenticationForm implements Serializable{
	
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	private String login;
	private String password;

}
