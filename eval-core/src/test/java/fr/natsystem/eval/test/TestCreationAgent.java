package fr.natsystem.eval.test;

import java.util.Date;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.natsystem.eval.dao.impl.CoeffContexteAgentDAO;
import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.BusinessUnit;
import fr.natsystem.eval.datamodel.Cycle;
import fr.natsystem.eval.datamodel.Habilitation;
import fr.natsystem.eval.datamodel.Role;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestCreationAgent {

	@Inject
	CoeffContexteAgentDAO cDAO;
	
	@Inject
	SessionFactory factory;
	
	@Test
	public void creationAgent() {
		BusinessUnit unite1 = new BusinessUnit("unite1", null);
		BusinessUnit unite2 = new BusinessUnit("unite2", null);
		Agent FRS1 = new Agent("Yann", "Badoister","6512471Y", null, null, cDAO.getByCode("N"));
		
		Agent FRS2 = new Agent("Nicolas", "Ongalere","7412004B", null, null, cDAO.getByCode("N"));
		Agent DU1 = new Agent("Gerald", "Glubenfeld","7412005C", null, unite1, cDAO.getByCode("FA"));
		Agent DU2 = new Agent("Franck", "Nouel","7412006D", null, unite2, cDAO.getByCode("DE"));
		Agent DPX1 = new Agent("Willy", "Matre","7412007E", DU1, unite1, cDAO.getByCode("N"));
		Agent DPX2 = new Agent("Philippe", "Mannin","7412009G", DU1, unite1, cDAO.getByCode("N"));
		Agent agent1 = new Agent("Romain", "Lemouneau","CP0123216", DPX1, unite1, cDAO.getByCode("N"));
		Agent agent2 = new Agent("Robert", "Bochon","DX0893210", DPX2, unite1, cDAO.getByCode("N"));
		Agent agent3 = new Agent("Marc", "Sistero","DX6541212", null, unite2, cDAO.getByCode("N"));
		Agent agent4 = new Agent("Hannibal", "Rufio","DX6520044", DPX1, unite1, cDAO.getByCode("N"));
		Agent agent5 = new Agent("Merlin", "Tolimar","DX8554123", DPX1, unite1, cDAO.getByCode("N"));

		Cycle cycle1 = new Cycle(new Date(), null);
		Role FRS = new Role("FRS");
		Role DU = new Role("DU");
		Role DPX = new Role("DPX");
		Habilitation hab1 = new Habilitation(FRS1, FRS);
		Habilitation hab2 = new Habilitation(FRS2, FRS);
		Habilitation hab3 = new Habilitation(DU1, DU);
		Habilitation hab4 = new Habilitation(DU2, DU);
		Habilitation hab5 = new Habilitation(DPX1, DPX);
		Habilitation hab6 = new Habilitation(DPX2, DPX);


		
		Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        
		session.saveOrUpdate(cycle1);

		session.saveOrUpdate(FRS);
		session.saveOrUpdate(DU);
		session.saveOrUpdate(DPX);
		
		session.saveOrUpdate(hab1);
		session.saveOrUpdate(hab2);
		session.saveOrUpdate(hab3);
		session.saveOrUpdate(hab4);
		session.saveOrUpdate(hab5);
		session.saveOrUpdate(hab6);



		session.saveOrUpdate(unite1);
		session.saveOrUpdate(unite2);
		
		session.saveOrUpdate(FRS1);
		session.saveOrUpdate(FRS2);
		session.saveOrUpdate(DU1);
		session.saveOrUpdate(DU2);
		session.saveOrUpdate(DPX1);
		session.saveOrUpdate(DPX2);

		session.saveOrUpdate(agent2);
		session.saveOrUpdate(agent1);
		session.saveOrUpdate(agent3);
		session.saveOrUpdate(agent4);
		session.saveOrUpdate(agent5);
		transaction.commit();
		session.close();
	
	}

}


