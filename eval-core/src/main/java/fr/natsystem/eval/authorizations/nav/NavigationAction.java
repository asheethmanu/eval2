package fr.natsystem.eval.authorizations.nav;

public class NavigationAction {
	
	private NavigationPart definition;
	private String url;
	public NavigationPart getDefinition() {
		return definition;
	}
	public void setDefinition(NavigationPart definition) {
		this.definition = definition;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
