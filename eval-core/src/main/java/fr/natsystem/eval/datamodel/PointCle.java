package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="POINT_CLE")
public class PointCle implements Serializable, Comparable<PointCle>{
	
	private static final long serialVersionUID = 1418447505406533527L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="POINT_CLE_ID")
	private int id;
	
	@Column(name="POINT_CLE_LIBELLE")
	private String libelle;
	
	@ManyToOne
	@JoinColumn(name="POINT_CLE_GRAVITE")
	private Gravite graviteRef;
	
	@ManyToOne
	@JoinColumn(name="POINT_CLE_THEME_REF")
	private Theme themeRef;
	
	@Column(name="POINT_CLE_ORDRE")
	private int ordre;

	public PointCle(String libelle, Theme theme, Gravite gravite, int ordre) {
		this.themeRef = theme;
		this.libelle = libelle;
		this.graviteRef = gravite;
		this.ordre = ordre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public PointCle() {
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Theme getThemeRef() {
		return themeRef;
	}

	public void setThemeRef(Theme themeRef) {
		this.themeRef = themeRef;
	}

	public Gravite getGraviteRef() {
		return graviteRef;
	}

	public void setGraviteRef(Gravite graviteRef) {
		this.graviteRef = graviteRef;
	}

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}

	@Override
	public int compareTo(PointCle pointCle) {
		return this.getOrdre() - pointCle.getOrdre();
	}

	
}
