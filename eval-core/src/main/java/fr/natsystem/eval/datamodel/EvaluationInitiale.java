package fr.natsystem.eval.datamodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="EVALUATION_INITIALE")
public class EvaluationInitiale implements Serializable{

	private static final long serialVersionUID = 6129628886256296007L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="EVALUATION_INITIALE_ID")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_INITIALE_PC_REF")
	private PointCle pointcleRef;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_INITIALE_AGENT_REF")
	private Agent agentRef;
	
	@Column(name="EVALUATION_INITIALE_DATE")
	private Date date;
	
	@ManyToOne
	@JoinColumn(name="EVALUATION_INITIALE_SAMI")
	private SAMI sami;
	
	@Column(name="EVALUATION_INITIALE_PRINCIPAL")
	private boolean evalPrincipale;

	
	public EvaluationInitiale() {
		super();
	}

	public EvaluationInitiale(PointCle pointcleRef, Agent agentRef, Date date,
			SAMI sami, boolean evalPrincipale) {
		super();
		this.pointcleRef = pointcleRef;
		this.agentRef = agentRef;
		this.date = date;
		this.sami = sami;
		this.evalPrincipale = evalPrincipale;
	}

	public PointCle getPointcleRef() {
		return pointcleRef;
	}

	public void setPointcleRef(PointCle pointcleRef) {
		this.pointcleRef = pointcleRef;
	}

	public Agent getAgentRef() {
		return agentRef;
	}

	public void setAgentRef(Agent agentRef) {
		this.agentRef = agentRef;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public SAMI getSami() {
		return sami;
	}

	public void setSami(SAMI sami) {
		this.sami = sami;
	}

	public boolean isEvalPrincipale() {
		return evalPrincipale;
	}

	public void setEvalPrincipale(boolean evalPrincipale) {
		this.evalPrincipale = evalPrincipale;
	}
	

}
