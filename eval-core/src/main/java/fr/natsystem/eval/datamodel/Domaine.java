package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="DOMAINE")
public class Domaine implements Serializable{
	
	private static final long serialVersionUID = -310830594488605614L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="DOMAINE_ID")
	private int id;
	
	@Column(name="DOMAINE_LIBELLE")
	private String libelle;
	
	@ManyToOne
	@JoinColumn(name="DOMAINE_ESPACE_REF")
	private Espace espaceRef;

	public Domaine() {
	}
	
	public Domaine(String libelle) {
		this.libelle = libelle;
	}

	public Domaine(String libelle, Espace espace) {
		this.libelle = libelle;
		this.espaceRef = espace;
	}

	public int getIdDomaine() {
		return id;
	}

	public void setIdDomaine(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Espace getEspaceRef() {
		return espaceRef;
	}

	public void setEspaceRef(Espace espaceRef) {
		this.espaceRef = espaceRef;
	}

}
