package fr.natsystem.eval.datamodel;

import java.math.BigDecimal;

public class SynthesePointCle {

	private int numPointCle;
	
	private String nomTheme;
	
	private String nomPointCle;
	
	private CoefficientContextePC coeffContextePC;
	
	private double minimumScore;
	
	private BigDecimal observedScore;
	
	private double maximumScore;
	
	private SAMI evaluation;
	
	private BigDecimal relativeScore;
	
	private SAMI evaluationInitiale;

	public SynthesePointCle() {
		super();
	}

	public SynthesePointCle(int numPointCle, String nomTheme, String nomPointCle, CoefficientContextePC coeffContextePC, double minimumScore, BigDecimal observedScore, double maximumScore,
			SAMI evaluation, BigDecimal relativeScore, SAMI evaluationInitiale) {
		this.setNumPointCle(numPointCle);
		this.setNomTheme(nomTheme);
		this.setNomPointCle(nomPointCle);
		this.setCoeffContextePC(coeffContextePC);
		this.minimumScore = minimumScore;
		this.observedScore = observedScore;
		this.maximumScore = maximumScore;
		this.evaluation = evaluation;
		this.relativeScore = relativeScore;
		this.evaluationInitiale = evaluationInitiale;
	}

	public double getMinimumScore() {
		return minimumScore;
	}

	public void setMinimumScore(double minimumScore) {
		this.minimumScore = minimumScore;
	}

	public BigDecimal getObservedScore() {
		return observedScore;
	}

	public void setObservedScore(BigDecimal observedScore) {
		this.observedScore = observedScore;
	}

	public double getMaximumScore() {
		return maximumScore;
	}

	public void setMaximumScore(double maximumScore) {
		this.maximumScore = maximumScore;
	}

	public SAMI getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(SAMI evaluation) {
		this.evaluation = evaluation;
	}

	public BigDecimal getRelativeScore() {
		return relativeScore;
	}

	public void setRelativeScore(BigDecimal relativeScore) {
		this.relativeScore = relativeScore;
	}

	public String getNomPointCle() {
		return nomPointCle;
	}

	public void setNomPointCle(String nomPointCle) {
		this.nomPointCle = nomPointCle;
	}

	public SAMI getEvaluationInitiale() {
		return evaluationInitiale;
	}

	public void setEvaluationInitiale(SAMI evaluationInitiale) {
		this.evaluationInitiale = evaluationInitiale;
	}

	public CoefficientContextePC getCoeffContextePC() {
		return coeffContextePC;
	}

	public void setCoeffContextePC(CoefficientContextePC coeffContextePC) {
		this.coeffContextePC = coeffContextePC;
	}

	public int getNumPointCle() {
		return numPointCle;
	}

	public void setNumPointCle(int numPointCle) {
		this.numPointCle = numPointCle;
	}

	public String getNomTheme() {
		return nomTheme;
	}

	public void setNomTheme(String nomTheme) {
		this.nomTheme = nomTheme;
	}

}
