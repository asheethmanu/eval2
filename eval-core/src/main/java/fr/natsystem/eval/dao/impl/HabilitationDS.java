package fr.natsystem.eval.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import fr.natsystem.eval.dao.IHabilitationRepository;
import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.Habilitation;
import fr.natsystem.eval.datamodel.Role;

@Component
public class HabilitationDS {

	@Inject
	IHabilitationRepository repository;

	/**
	 * gets the authorizations for a given DPX agent
	 * 
	 * @param agent
	 * @return a list of authorization (Habilitation)
	 */
	public Habilitation getHabilitationDPXParAgent(Agent agent) {
		List<Habilitation> habilitations = this.repository.findHabilitationsForAgentByRoleName(agent, "DPX");
		if (habilitations == null || habilitations.isEmpty()) {
			return null;
		}
		return habilitations.get(0);

	}

	/**
	 * gets the roles for a given Agent
	 * @param agent
	 * @return a list of Roles
	 */
	public List<Role> getAgentRoles(Agent agent) {

		Collection<Habilitation> habilitations = getAgentHabilitations(agent);
		List<Role> roles = new ArrayList<Role>();
		for (Habilitation habilitation : habilitations) {
			roles.add(habilitation.getRole());
		}
		return roles;
	}

	/**
	 * gets the habilitations (authorizations) for a given agent
	 * @param agent
	 * @return a list of Habilitations
	 */
	public List<Habilitation> getAgentHabilitations(Agent agent) {
		Habilitation habilitation = new Habilitation();
		habilitation.setAgent(agent);
		return this.repository.findAll(Example.of(habilitation));
	}

	/**
	 * Deletes an authorization
	 * @param hab the habilitation to remove
	 */
	public void deleteHabilitation(Habilitation hab) {
		this.repository.delete(hab);

	}


	/**
	 * Saves an authorization
	 * @param hab the habilitation to save
	 */
	public void saveHabilitation(Habilitation hab) {
		this.repository.save(hab);
	}
}
