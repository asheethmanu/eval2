package fr.natsystem.eval.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import fr.natsystem.eval.dao.IBusinessUnitDAO;
import fr.natsystem.eval.dao.IBusinessUnitRepository;
import fr.natsystem.eval.datamodel.BusinessUnit;
import fr.natsystem.eval.datamodel.DirectionFret;

@Component
public class BusinessUnitDS implements IBusinessUnitDAO {

	@Autowired
	IBusinessUnitRepository repository;
	
	@Override
	public List<BusinessUnit> getAllBusinessUnits() {
		return this.repository.findAll();
	}

	public List<BusinessUnit> getBusinessUnitParNom(String string) {
		return this.repository.findAll(Example.of(new BusinessUnit(string, null)));
	}

	public List<BusinessUnit> getBusinessUnitParDF(DirectionFret df) {
		return this.repository.findAll();
	}
}
