package fr.natsystem.eval.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import fr.natsystem.eval.datamodel.Agent;


@Component
public interface IAgentDAO{
	
	public List<Agent> getListeAgentsParAgentRef(Agent agentRef);

	public List<Agent> getListeAgents();

	public Agent getByCP(String string);
}
