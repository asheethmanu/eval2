/**
 * 
 */
package fr.natsystem.eval.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.Habilitation;

@Repository
public interface IHabilitationRepository extends JpaRepository<Habilitation, Long> {

	/**
	 * get the habilitations for a an agent and a given role
	 * @param agent
	 * @param roleName
	 * @return a list of habilitation
	 */
	List<Habilitation> findHabilitationsForAgentByRoleName(@Param("agent") Agent agent,
			@Param("roleName") String roleName);

}
