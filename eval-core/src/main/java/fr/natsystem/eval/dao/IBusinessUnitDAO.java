package fr.natsystem.eval.dao;

import java.util.List;

import fr.natsystem.eval.datamodel.BusinessUnit;


public interface IBusinessUnitDAO {
	public List<BusinessUnit> getAllBusinessUnits();

}
